# Information about this fork

Literal translation of scaffold templates from EBB to HAML.
Original idea from [Paul Barry's article on custom genrators][OriginalIdea]

## Installation

1. Generate your new rails application:

        rails ApplicationName
        cd ApplicationName

1. Edit "Gemfile" and add "gem haml" to the gem list
1. Either

        gem install haml

    ...or...

        bundle install

1. Edit config/application.rb and add the following:

        config.generators do |g|
            g.template_engine :haml
        end


1. Either 

        git clone git://github.com/lamnk/rails3_haml_scaffold_generator.git lib/generators/haml

    ...or...

        git submodule add git://github.com/lamnk/rails3_haml_scaffold_generator.git lib/generators/haml
  
1. Create stuff with:

        rails generate controller ControllerName index
        rails generate mailer ExamplesNotifications
        rails generate scaffold FancyModel
    
    ... or if you like to mix it up with ERB, ignore step 5 and use ...

        rails generate haml:controller ControllerName index
        rails generate haml:mailer ExamplesNotifications
        rails generate haml:scaffold FancyModel

[OriginalIdea]: http://paulbarry.com/articles/2010/01/13/customizing-generators-in-rails-3
